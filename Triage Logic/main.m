//
//  main.m
//  Triage Logic
//
//  Created by Richard Morgan on 18/01/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
