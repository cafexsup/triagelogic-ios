//
//  ViewController.m
//  Triage Logic
//
//  Created by Richard Morgan on 18/01/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import "ViewController.h"
#import <ACBClientSDK/ACBUC.h>


@interface ViewController ()

@end

@implementation ViewController

NSString *dest = @"";

static ACBUC *acbuc;

+ (ACBUC *) getACBUC
{
    return acbuc;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // FCSDK CALL SETUP - you need to point this urlString to your own session provisioning server
        
        NSString *urlString = [NSString stringWithFormat: @"%@", @"https://f80812e5.ngrok.io/session.php"];
        
        
        NSLog(@"::urlString: %@", urlString);
        
        
        NSURL *sessionUrl = [NSURL URLWithString:urlString];
        
        NSLog(@"::Session: %@", sessionUrl);
        
        NSString *sessionID = [NSString stringWithContentsOfURL:sessionUrl encoding:NSASCIIStringEncoding
                                                          error:nil];
        NSString *trimmed = [sessionID stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSLog(@"::Session: %@", sessionID);
        
        NSLog(@"::Trimmed: %@", trimmed);
        
        // now init the with the session id
        acbuc = [ACBUC ucWithConfiguration:sessionID delegate:self];
        
        [acbuc setNetworkReachable:YES];
        [acbuc  acceptAnyCertificate:YES];
        
        [acbuc startSession];
        
        NSLog(@"::Session: %@", sessionID);
        
    });
    

}




-(void)makeCall: (NSString *)destination
{
    NSLog(@"this is the destination to call: %@", destination);
    
    dest = destination;
    
   
}

-(void)ucDidStartSession:(ACBUC *)uc
{
    
    NSLog(@"::ucDidStartSession");
    
    self.acbuc = [ViewController getACBUC];
    
    ACBClientPhone* phone = self.acbuc.phone;
    phone.delegate = self;
    phone.previewView = self.local;
    
    [ACBClientPhone requestMicrophoneAndCameraPermission:true video:true];
    
    ACBClientCall* call = [phone createCallToAddress:dest audio:YES video:YES delegate:self];
    call.videoView = self.remote;
    
    
}



-(void)ucDidFailToStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidFailToStartSession");
    
    NSLog(@" uc %@", uc);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
