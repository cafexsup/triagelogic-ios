//
//  AppDelegate.h
//  Triage Logic
//
//  Created by Richard Morgan on 18/01/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

