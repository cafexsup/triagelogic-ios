//
//  ViewController.h
//  Triage Logic
//
//  Created by Richard Morgan on 18/01/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ViewController : UIViewController <UIWebViewDelegate,ACBClientCallDelegate, ACBUCDelegate, ACBClientPhoneDelegate>

-(void)makeCall:(NSString *)destination;

extern NSString * dest;

@property (strong, nonatomic) IBOutlet UIView *remote;
@property (strong, nonatomic) IBOutlet UIView *local;


@property (weak, nonatomic) ACBUC *acbuc;

/// Config

+ (ACBUC *) getACBUC;


@end

