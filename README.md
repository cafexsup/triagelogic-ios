

#Instructions

* Include dependencies and the FCSDK 3.0.6 iOS Framework, within xCode as per the developer Documentation
* Setup your session provisioning server as per recommeneded arch / web training tutorials
* Edit the ViewController.m file to point the the url for this service
* From Safari or another application (messages) open with the following url iOSTriageLogic://?destination=1002
